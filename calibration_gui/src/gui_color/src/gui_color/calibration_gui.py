#!/usr/bin/python

"""
Created on 7th June 2016

@author: abhishek , d.patel@interproductec.com

This node meets the gui requirement for color calibration tool.

"""
import sys
sys.path.insert(0, "/opt/opencv3/lib/python2.7/dist-packages")

try:
    # for python 2
    import Tkinter
    import tkFileDialog
except ImportError:
    # for python3
    import tkinter as Tkinter
    import filedialog as tkFileDialog
import cv2
from gui_color.calibrate import calibrate
from compute_transform import transform_colors_LAB
from PIL import Image, ImageTk
from sensor_msgs.msg import Image as image_type
import numpy as np
import os
import logging


class CalibrationGui(Tkinter.Tk):

    def __init__(self, parent, logger=None):
        """Initialize"""
        np.set_printoptions(precision=3)  # print option for numpy
        Tkinter.Tk.__init__(self, parent)
        self.coordinates = list()
        self.values = list()
        self.play_flag = True
        self.click_count = 0
        self.FIRST_IMAGE = True
        self.create_gui()
        self._log = logger or logging.getLogger(__name__)

    def __enter__(self):
        return self

    def __exit__(self, *args):
        pass

    def create_gui(self):
        """Helps to create GUI"""
        FRAME_ASPECT = 4.0 / 3.0
        screen_width = self.winfo_screenwidth()

        screen_height = self.winfo_screenheight()
        frame_height = screen_height / 2
        frame_width = int(frame_height * FRAME_ASPECT)
        if frame_width > screen_width:
            frame_width = screen_width / 2
            frame_height = int(frame_width / FRAME_ASPECT)

        master_frame = Tkinter.Frame(
            self, width=frame_width, height=frame_height).grid()

        self.canvas_width = int(frame_width * (5.0 / 6.0))
        self.canvas_height = int(frame_height * (2.0 / 3.0))
        self.canvas_raw = Tkinter.Canvas(
            master_frame, width=self.canvas_width, height=self.canvas_height)
        self.canvas_corrected = Tkinter.Canvas(
            master_frame, width=self.canvas_width,  height=self.canvas_height)
        self.canvas_raw.grid(row=0, column=0, ipadx=10)
        self.canvas_corrected.grid(row=0, column=1, ipadx=10)

        self.canvas_raw.bind('<ButtonRelease>', self.draw_markers)

        button_frame = Tkinter.LabelFrame(
            master_frame)
        button_frame.grid(row=1, column=0)
        button_width = frame_width / 50
        self.button_play = Tkinter.Button(
            button_frame, text="Pause", width=button_width, height=1, command=self.on_play)
        self.button_play.grid(row=1, column=1)

        button_accept = Tkinter.Button(
            button_frame, text="Accept", width=button_width, height=1, command=self.on_accept)
        button_accept.grid(row=1, column=2)

        button_reset = Tkinter.Button(
            button_frame, text="Reset", width=button_width, height=1, command=self.on_reset)
        button_reset.grid(row=1, column=3)

        button_save = Tkinter.Button(
            button_frame, text="Save", width=button_width, height=1, command=self.on_save)
        button_save.grid(row=1, column=4)

        self.coordinates_frame = Tkinter.LabelFrame(
            master_frame, text="Coordinates", padx=5, pady=5)
        self.coordinates_frame.grid(row=1, column=1)
        corner_names = [
            'Top Left: ', 'Top Right: ', 'Down Right: ', 'Down Left: ']
        for i, label in enumerate(corner_names):
            Tkinter.Label(self.coordinates_frame, text=label,
                          width=button_width, height=1).grid(row=i, column=0)
            Tkinter.Label(self.coordinates_frame, text="(0,0)").grid(
                row=i, column=1)

        # For resize row and column when window is resized
        self.grid_columnconfigure(0, weight=1)

    # toggle the play/pause flag
    def on_play(self):
        """ Callback for play/pause button"""
        if(self.play_flag):
            self.button_play["text"] = "Play"
            self.play_flag = False
        else:
            self.button_play["text"] = "Pause"
            self.play_flag = True

    # get co-ordinates of selected points
    def draw_markers(self, event):
        """ Gets the value of x and y """
        if not self.play_flag:
            x, y = event.x, event.y
            self.create_circle(
                x, y, 5, fill="blue", outline="#DDD", width=2, tags="markers")
            self.click_count += 1
            self.coordinates.append((event.x, event.y))
            # check if polygon is already drawn
            Tkinter.Label(self.coordinates_frame, text="({0},{1})".format(x, y)).grid(
                row=self.click_count - 1, column=1)
            if(self.click_count == 4):
                self.canvas_raw.delete("poly")
                self.canvas_raw.delete("markers")
                self.canvas_raw.create_polygon(
                    tuple(self.coordinates), fill='', outline='black', width=4, tags='poly')
                self.click_count = 0
                self.values = self.coordinates
                self.coordinates = []

            # if new point is selected after the polygon is already  drawn
            if (self.canvas_raw.find_withtag("poly") and self.click_count == 1):
                self.on_reset()
                self.draw_markers(event)

    # callback to draw image on canvas
    def draw_image(self, cv_image):

        aspect_ratio = float(cv_image.shape[1]) / cv_image.shape[0]

        # calculating height for fixed width 800(width of canvas)
        new_height = int(self.canvas_width / aspect_ratio)
        full_image = cv_image
           # for the case in which the new height is grater than canvas height
        if (new_height > self.canvas_height):
            new_width = int(self.canvas_height * aspect_ratio)
            self.resized_image = cv2.resize(
                cv_image, (new_width, self.canvas_height))
        else:
            self.resized_image = cv2.resize(
                cv_image, (self.canvas_width, new_height))

        cv_image = cv2.cvtColor(self.resized_image, cv2.COLOR_BGR2RGB)
        self.img_frame = ImageTk.PhotoImage(Image.fromarray(cv_image))

        if(self.FIRST_IMAGE):
            self.raw_img_canvas = self.canvas_raw.create_image(
                0, 0, anchor='nw', image=self.img_frame, tags='full_image')
            self.corrected_img_canvas = self.canvas_corrected.create_image(
                0, 0, anchor='nw', image=None, tags='full_image')

            self.img_frame_old = self.img_frame
            self.full_image_old = full_image
            self.full_image = full_image
            self.FIRST_IMAGE = False

        elif(self.play_flag):
            self.canvas_raw.itemconfig(
                self.raw_img_canvas, image=self.img_frame)
            # keep copy of last full image before pause

            self.img_frame_old = self.img_frame
            self.full_image_old = full_image
        else:
            self.full_image = self.full_image_old
            self.canvas_raw.itemconfig(
                self.raw_img_canvas, image=self.img_frame_old)

    def on_accept(self):
        assert(len(self.values) == 4)
        self.img_corrected_cv, self.trans_matrix = calibrate(
            self.resized_image, np.asarray(self.values))
        img_corrected_cv = cv2.cvtColor(
            self.img_corrected_cv, cv2.COLOR_BGR2RGB)
        self.img_corrected = ImageTk.PhotoImage(
            Image.fromarray(img_corrected_cv))
        self.canvas_corrected.itemconfig(
            self.corrected_img_canvas, image=self.img_corrected)
        self._log.info(" transformation matrix \n%s" % self.trans_matrix)

    def on_reset(self):
        self.canvas_raw.delete("poly")
        self.canvas_raw.delete("markers")
        for i in range(4):
            Tkinter.Label(self.coordinates_frame, text="    (0,0)    ").grid(
                row=i, column=1)
        self.values = []
        self.coordinates = []
        self.click_count = 0

    def on_save(self):
        assert(self.trans_matrix is not None)
        full_corrected = transform_colors_LAB(
            self.full_image, self.trans_matrix)
        if(not self.play_flag):
            filename = tkFileDialog.asksaveasfilename(
                defaultextension=".jpg",  initialdir=os.path.expanduser("~/Pictures"))
            if filename is None:  # asksaveasfile return `None` if dialog closed with "cancel".
                return
            cv2.imwrite(filename, full_corrected)

    def create_circle(self, x, y, r, **kwargs):
        """Helps to create circle"""
        self.canvas_raw.create_oval(x - r, y - r, x + r, y + r, **kwargs)

if __name__ == "__main__":

    if (len(sys.argv) > 1):
        image = cv2.imread(sys.argv[1])
    else:
        image = cv2.imread(os.path.expanduser("~/test.jpg"))
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    with CalibrationGui(None, logger) as app:
        # set title
        app.title("Images")
        app.draw_image(image)
        # run in loop to hold the window
        app.mainloop()
