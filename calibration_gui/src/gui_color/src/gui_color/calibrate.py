#/usr/bin/env python
import sys
sys.path.insert(0, "/opt/opencv3/lib/python2.7/dist-packages")
import cv2
import numpy as np
from compute_transform import *
from extract_chart import *
from generate_MBCC import rotate90_chart
import os

def calibrate(img, corner_pts):
    
    #extract MBCC from image
    roi = extract_roi(img, corner_pts)
    cv2.imshow("xtracted",roi)
    #extract colored sqaures from MBCC
    img_s = get_color_roi(roi)
    #get average color for each square
    colors_measured = get_avg_color(img_s).astype(np.uint8)
    # convert to LAB 
    colors_measured_LAB = convert_BGR2LAB(colors_measured.reshape((24,1,3)))
    #get true color values for MBCC
    colors_true = np.genfromtxt(os.path.expanduser('~/data/MBCC_colors.txt'), delimiter = ',')

    #rotate the chart by 90 deg
    colors_true = rotate90_chart(colors_true)
    colors_true_LAB = convert_BGR2LAB(colors_true.reshape((24,1,3)))
    #T = CLS_Boyd2(colors_measured_LAB.reshape((24,3)), colors_true_LAB.reshape((24,3)))
    T = gradient_descent(colors_measured_LAB.reshape((24,3)), colors_true_LAB.reshape((24,3)))

    #mbcc_corrected = transform_colors_LAB(mbcc_measured, T)
    
    img_transformed = transform_colors_LAB(img, T)
    #img_transformed = cv2.GaussianBlur(img_transformed , (3,3),0,0)
    
    return img_transformed, T

if __name__ == "__main__":
    import os

    img = cv2.imread("../../Images/image1.png")
    corner_pts =np.asarray([(1016,533), (1274,552), (1252,930), (1000,889)])
    img_cal = calibrate(img,corner_pts)
    cv2.imshow("calibrated",img_cal)
    cv2.waitKey(0)
