#!/usr/bin/env python

import cv2
import numpy as np
from extract_chart import convert_BGR2LAB
from scipy.optimize import minimize
import logging

def transform_colors_LAB( img, T):
    img_LAB = convert_BGR2LAB(img)
    img_cols = img_LAB.reshape((-1,3))
    img_corrected  = np.dot( img_cols, T) 
    img_BGR  = cv2.cvtColor(img_corrected.reshape(img.shape) , cv2.COLOR_LAB2BGR)
    img_BGR = ((img_BGR)*255).astype(np.uint8) 
    return img_BGR 

# constrained least square (1 constrain)
def CLS_Boyd(source_color, target_color):
    #constrain for KKT equations
    C = source_color[0].reshape((1,3)) #White Color for constraint
    d = target_color[0].reshape((1,3)) #White Color for constraint
    source_color = source_color[1:]
    target_color = target_color[1:]
    m11 = 2* np.dot(source_color.T,source_color).astype(np.float32)
    m12 = C.T
    m21 = C 
    #m11 = m11 + np.dot(m12, m21)
    m22 = np.zeros((1,1))
    M1 = np.append(m11, m12, axis = 1)
    M2 = np.append(m21, m22 , axis = 1)
    M  = np.append(M1, M2 , axis = 0)
    #print(M.shape)
    M_inv = np.linalg.pinv(M)
    T = np.zeros((4,3)).astype(np.float32)
    for i in range (3):
        b = target_color[:,i].astype(np.float)
        b = b.reshape((-1,1))
        y1 = 2*np.dot(source_color.T.astype(np.float) , b)
        y2 = (d[0][i]).reshape((1,1))
        Y = np.append(y1, y2, axis = 0)

        T[:,i] = np.dot(M_inv, Y).ravel()
    T = T[0:3,0:3]
    return T


def constraint(T, C, D):
    diff = np.dot(C,T) -D
    return diff 


def gradient(T,source_color, target_color):
    
    diff = np.dot(source_color, T) - target_color
    grad =2* np.dot(source_color.T, diff)
    return grad

def err_fun(T,source_color, target_color):
    diff = np.dot(source_color, T) - target_color
    err = np.dot(diff.T, diff)
    logging.debug("err %s"%err)
    return err

def gradient_descent( source_color, target_color):
    
    #c1 = np.ones((1,3))*255
    #c1 = convert_BGR2LAB(c1.reshape(1,1,3))
    #C = c1.reshape((1,3))
    
    #d1 = np.ones((1,3))*255
    #d1 = convert_BGR2LAB(d1.reshape(1,1,3))
    #D = d1.reshape((1,3))

    C = source_color[0].reshape((1,3)) #White Color for constraint
    D = target_color[0].reshape((1,3)) #White Color for constraint
    
    #T_init = np.random.rand(3,3)
    T_init = np.eye(3)
    T = np.zeros((3,3)).astype(np.float32)
    for i in range(3):
        cons = {'type' : 'eq', 'fun': constraint , 'args':(C,D[:,i])}
        res = minimize(err_fun ,T_init[:,i],
                    args=(source_color,target_color[:,i]),
                    method='SLSQP', jac=gradient, 
                    constraints = cons, 
                    options= {'disp':False , 'eps':10e-2})
        T[:,i] = res.x
    return T

# constrained least square (2 constrain)
def CLS_Boyd2(source_color, target_color):
    #constrain for KKT equations
    c1 = np.ones((1,3))*255
    c2 = source_color[0].reshape((1,3)) #White color
    c1 = convert_BGR2LAB(c1.reshape(1,1,3))
    c1 = c1.reshape((1,3))
    C = c1 
    #C = np.append(c1 , c2 , axis =0)
    
    d1 = np.ones((1,3))*255
    d2 = target_color[0].reshape((1,3)) #White color
    d1 = convert_BGR2LAB(d1.reshape(1,1,3))
    d1 = d1.reshape((1,3))
    D = d1
    #D = np.append(d1, d2 , axis =0)
    

    m11 = 2 * np.dot(source_color.T,source_color).astype(np.float32)
    m12 = C.T
    m21 = C 
    m22 = np.zeros((1,1))
    M1 = np.append(m11, m12, axis = 1)
    M2 = np.append(m21, m22 , axis = 1)
    M  = np.append(M1, M2 , axis = 0)
    M_inv = np.linalg.pinv(M)
    T = np.zeros((4,3)).astype(np.float32)
    for i in range (3):
        b = target_color[:,i].astype(np.float)
        b = b.reshape((-1,1))
        y1 = 2 * np.dot(source_color.T.astype(np.float) , b)
        y2 = (D[0][i]).reshape((1,1))
        #y31 = (D[1][i]).reshape((1,1))
        #y2 = np.append(y21,y31, axis =0 ) 
        Y = np.append(y1, y2, axis = 0)

        T[:,i] = np.dot(M_inv, Y).ravel()

    T = T[0:3,0:3]
    return T

