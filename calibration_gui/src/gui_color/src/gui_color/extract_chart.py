#!/usr/bin/env python

import cv2
import numpy as np
from generate_MBCC import gamma_correct
#dimension of Macbeth chart in cm
HEIGHT = 29
WIDTH = 21
SQUARE_BORDER = 0.5
CHART_BORDER_TB = 1
CHART_BORDER_LR = 1.5
COLOR_SQAUARE = 4 

# pixel to cm scale

SCALE = 20

def get_color_roi(img, rows = WIDTH * SCALE, cols = HEIGHT * SCALE,  scale = SCALE):
    img_scaled = cv2.resize(img, (rows, cols))
    square_size = COLOR_SQAUARE * SCALE
    x = int(CHART_BORDER_LR * SCALE)
    y = int(CHART_BORDER_TB * SCALE)    
    colored_squares = []
    buf = 1 * SCALE
    for i in range(6):
        for j in range(4): 
            color_roi = img_scaled[y + buf : y+square_size - buf, x + buf :x + square_size - buf]
            colored_squares.append(color_roi)
            x = int(x + square_size + SQUARE_BORDER * SCALE)
        y = int(y + square_size + SQUARE_BORDER * SCALE)
        x = int(CHART_BORDER_LR * SCALE)
    return np.asarray(colored_squares)

def get_avg_color(subimages):
    colors = []
    for s in subimages:
        # LAB colorspace
        #s = cv2.cvtColor(s, cv2.COLOR_BGR2LAB)
        col_mat = s.reshape((-1,3))
        avg = np.median(col_mat.astype(np.float32),axis = 0)
        colors.append(avg.astype(np.uint8)) 
    return np.asarray(colors)


def convert_BGR2LAB(img):
    img_LAB = cv2.cvtColor(img.astype(np.uint8), cv2.COLOR_BGR2LAB).astype(np.float32)
    img_LAB[:,:,0] = img_LAB[:,:,0] * (100/255.0)
    img_LAB[:,:,1] = img_LAB[:,:,1] - 128
    img_LAB[:,:,2] = img_LAB[:,:,2] - 128
    return img_LAB

def extract_roi(img, src_corner_pts):
    
    assert len(src_corner_pts == 4)
    
    mask = np.zeros(img.shape[0:2])
    mask = cv2.fillPoly( mask, np.array([src_corner_pts],'int32'), 255)
    chart_roi = cv2.bitwise_and(img, img, mask = mask.astype(np.uint8))
    x,y,w,h = cv2.boundingRect(src_corner_pts)
    dst_corner_pts = np.asarray([(x,y), (x+w,y), (x+w,y+h), (x, y+h)]) 
    img_chart = chart_roi[y:y+h,x:x+w]
   
    #translate points to origin    
    dst_corner_pts = dst_corner_pts - np.asarray((x,y))
    src_corner_pts = src_corner_pts - np.asarray((x,y))
     
    transform_matrix = cv2.getPerspectiveTransform(src_corner_pts.astype(np.float32), dst_corner_pts.astype(np.float32))
    
    #compute destination image dimensions
    src_transformed_pts = np.dot(np.append(src_corner_pts,np.ones((4,1)),axis=1), transform_matrix)   
    src_transformed_pts = np.multiply(1.0/src_transformed_pts [:,-1],src_transformed_pts.T).T
    src_max_x,src_max_y = np.max(src_transformed_pts[:,0]),np.max(src_transformed_pts[:,1]) 
    
    chart_warp = cv2.warpPerspective(img_chart, transform_matrix, (int(src_max_x), int(src_max_y)))
    #chart_warp = gamma_correct(chart_warp, gamma=(1/2.2))
    return chart_warp


