#!/usr/bin/env python

import numpy as np
import cv2 

HEIGHT = 29
WIDTH = 21
SQUARE_BORDER = 0.5
CHART_BORDER_TB = 1
CHART_BORDER_LR = 1.5
COLOR_SQAUARE = 4
GAMMA = 1.0
# pixel to cm scale
SCALE = 20

def generate_mbcc(colors, rows = WIDTH * SCALE, cols = HEIGHT * SCALE,  scale = SCALE):
    assert len(colors) == 24
    mbcc = np.zeros((cols, rows, 3)).astype(np.float32)
    square_size = int(COLOR_SQAUARE * SCALE)
    x = int(CHART_BORDER_LR * SCALE)
    y = int(CHART_BORDER_TB * SCALE)
 
    ROW = 6
    COL = 4
    for i in range (ROW):
        for j in range (COL):
            mbcc[y : y+square_size , x :x + square_size,0 ] = colors[i*COL+j][0]
            mbcc[y : y+square_size , x :x + square_size,1 ] = colors[i*COL+j][1]
            mbcc[y : y+square_size , x :x + square_size,2 ] = colors[i*COL+j][2]
            x = int(x + square_size + SQUARE_BORDER * SCALE)
            #print colors[i*COL+j] #, colors[i*j][0]
        y = int(y + square_size + SQUARE_BORDER * SCALE)
        x = int(CHART_BORDER_LR * SCALE)
    #LAB color space
    #mbcc= ((cv2.cvtColor(mbcc, cv2.COLOR_LAB2BGR))*255)
    #apply Gamma correction
    #mbcc = gamma_correct(mbcc, gamma=GAMMA)
    
    return mbcc.astype(np.uint8)

def gamma_correct(image, gamma=1.0):
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255  for i in np.arange(0, 256)]).astype(np.uint8)
 
    # apply gamma correction using the lookup table
    return cv2.LUT(image, table)

def rotate90_chart(colors, white_position = 'bl'):
    assert len(colors) == 24
    if white_position == 'bl':
        mbcc_bgr_grid = np.zeros((6,4,3))
        mbcc_rgb_grid = colors.reshape((4,6,3))
        mbcc_r = np.flipud(mbcc_rgb_grid[:,:,0]).T
        mbcc_g = np.flipud(mbcc_rgb_grid[:,:,1]).T
        mbcc_b = np.flipud(mbcc_rgb_grid[:,:,2]).T
        #convert RGB to BGR
        mbcc_bgr_grid[:,:,0] = mbcc_b
        mbcc_bgr_grid[:,:,1] = mbcc_g
        mbcc_bgr_grid[:,:,2] = mbcc_r
        return mbcc_bgr_grid.reshape((-1,3)).astype(np.uint8)
