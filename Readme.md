# Color Calibration Tool
The tools aims at computing the color calibration parameters.

## Execution

Launch the dummy image publish node
```sh
$ roslaunch gui_color dummy_publish.launch #publishes dummy image on topic /oven1/image0/hdr
```
Launch calibration gui
```sh
roslaunch gui_color gui.launch
```

The Gui should display the image published by the dummy_publish node .
### Getting  Color Corrected Image
In order to get the color-corrected image execute the following steps. 
1) To select the color checker, press the *Pause* button. 
2) Please select the four corners in the following order:
    
        Top- left -> top-right -> bottom-right -> bottom left 
(From top-left, clockwise)
3) Once the four points are selected, a quadrilateral should be drawn connecting these four points. 
4) Press *Accept* to see the color-corrected image. 
5) If you are not satisfied by the selection of points at any point, press *Reset* to clear the selected points. 
6) The result can be saved using the *Save* button. 

There are more images in the *Images* directory, for testing. To test with another image change the following line in 
*dummy_publisher* node (TO DO: make change using launch file) 
```
image_path = self.rospack.get_path('gui_color')+'/Images/image3.png'
```

